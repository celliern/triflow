.. _model:

Model
=====

The :py:class:`skfdiff.Model` object contains 5 main methods :

- :py:meth:`skfdiff.Model.F`
- :py:meth:`skfdiff.Model.J`,
- :py:meth:`skfdiff.Model.U_from_fields`,
- :py:meth:`skfdiff.Model.fields_from_U`
- :py:meth:`skfdiff.Model.Fields`

The two first are referred as :ref:`model routines <model_routines>`, the next
two are provided by the :ref:`grid builder <grid_builder>` while the later is
an helper method that will build the :py:class:`xarray.Dataset` adapted for the
routines.

As you can easily create a :py:class:`Model` from scratch by providing these
methods. This is a way to solve a specialized problem and still have access to
all the scikit-fdiff goodies. You will be able to use the scikit-fdiff
:ref:`Simulation` even if you skip its discretisation step.

In that case, :py:meth:`skfdiff.Model.J` is the only optional method. If you
choose to skip the Jacobian computation, the implicit solvers will be
unavailable.

.. see the example STUFF to see how to implement a custom model.

The scikit-fdiff model transform a system of partial differential equations
to the two routines :py:meth:`skfdiff.Model.F` :py:meth:`skfdiff.Model.J`,
passing through the following steps:

- transform the scikit-fdiff pseudo-language to :py:mod:`sympy` expressions

.. code-block:: python

  >>> from skfdiff import PDEquation
  >>> pde = PDEquation("dxxU + dyyU", "U(x, y)")
  >>> pde.symbolic_equation
  Derivative(U(x, y), (x, 2)) + Derivative(U(x, y), (y, 2))

- discretize the :py:mod:`sympy` expressions using finite difference.

.. code-block:: python

  >>> pde = PDEquation("dxxU", "U(x)")
  >>> pde.fdiff.simplify()
  (U[x_idx + 1] + U[x_idx - 1] - 2*U[x_idx])/dx**2

- Apply both precedent step for each equation of the system, and do the same
  for the boundary conditions.
- Find the domains where the different boundary condition and where the main
  equations have to be applied.
- For each of this domain, find the symbolic condition as well as the
  associated discrete equation.
- Build the final discrete piecewise function :math:`F(U_i)` as well as the
  associated discrete piecewise jacobian.
- Use this symbolic representation of the system to build a numerical routine
  that, given a :py:obj:`Fields`, will compute both :math:`F(U)` and
  :math:`J(U)`. This is the role of the :ref:`backends <backends>`.

.. _skfdiff_pseudolanguage:

Skfdiff pseudo language 
-----------------------

The mathematical pseudo-language used by scikit-fdiff is the same as the one
understood by sympy, with some addition.

- :code:`dxU` will be interpreted as :code:`Derivative(U, x)`. It will be the
  same for :code:`dyyU`, :code:`dxyV` ...
- :code:`dx(U ** 2 + V)` will be interpreted as
  :code:`Derivative(U ** 2 + V, x).doit()`.
- :code:`Dx(U ** 2 + V)` will be interpreted as
  :code:`Derivative(U ** 2 + V, x)`. This means that the derivative will not be
  expanded before discretisation, keeping the equation structure.

.. _model_routines:

Routines
--------

The two routines :py:meth:`skfdiff.Model.F` and :py:meth:`skfdiff.Model.J` will
compute the evolution vector :math:`\frac{\partial U}{\partial t} = F(U)` and
its associated Jacobian matrix:

.. math::

  J(U) =
  \begin{bmatrix}
  \frac{\partial F_0}{\partial U_0} & \cdots & \frac{\partial F_0}{\partial U_j} & \cdots & \frac{\partial F_0}{\partial U_N} \\
  \vdots & \ddots & & &\vdots\\
  \frac{\partial F_i}{\partial U_0} &  & \frac{\partial F_i}{\partial U_j} &  & \frac{\partial F_i}{\partial U_N} \\
  \vdots & & & \ddots &\vdots\\
  \frac{\partial F_N}{\partial U_0} & \cdots & \frac{\partial F_N}{\partial U_j} & \cdots & \frac{\partial F_N}{\partial U_N}
  \end{bmatrix}

Both take a :py:obj:`fields`, an optional :py:obj:`t` that default to
:py:obj:`t=0` and an optional callable :py:obj:`hook` (described in the
:ref:`hook` section).

.. _backends:

Backends
++++++++

The backends take the symbolic discrete system and build a (if possible) fast
numerical routine to compute :math:`F` and :math:`J`.

For now, two of them are implemented : the first one use :py:mod:`numpy`, the
second one use :py:mod:`numba`. The last one should have better performance,
but is more complex to install (conda installation is strongly recommended).
It can also fail on the most complex models.

As we have access to a piecewise symbolic equation in a discrete form, we can
easily build such routine, that will compute the proper value for each domain.

.. _grid_builder:

Grid builder
------------

We take multi-dimensional array as input. The ODE solvers will work with 1D
solution vector :math:`U` and with 1D evolution vector :math:`F(U)`. We have
to provide the application that transform the complex fields Dataset to a
column vector : such operation is names `vectorization
<https://en.wikipedia.org/wiki/Vectorization_(mathematics)>`_. We also have to
provide the inverse application, so called `Matricization or Tensor reshaping
<https://en.wikipedia.org/wiki/Tensor_reshaping>`_.

We can do that in a trivial way by flattening every input array, then
concatenate them. This will be good enough for explicit solvers where we
only need the vector :math:`F(U)`. But, as we compute the associated Jacobian,
we will quickly observe that this vectorization is sub-optimal. With a simple
couplage between two 1D equation, the way we flatten and concatenate them will
have a strong impact on the structure of the system we will have to solve.

To have a better understanding, we can implement a simple 1D system via numpy :

.. math::

  \begin{cases}
    \frac{\partial u}{\partial t} = \partial_{xx}u \times \partial_{xx}v\\
    \frac{\partial v}{\partial t} = \partial_{xx}u + \partial_{xx}v
  \end{cases}

with periodic boundary condition.

The first attempt will use a simple concatenation : we will pile up all the
:math:`u` then all the :math:`v` as :

.. math::

  U = \begin{bmatrix}
        u_0\\
        \vdots\\
        u_i\\
        \vdots\\
        u_N\\
        v_0\\
        \vdots\\
        v_i\\
        \vdots\\
        v_N
      \end{bmatrix}

We can write some useful function, the first to approximate the jacobian matrix
via finite difference (in an very inefficient way, but sufficient for teaching
purpose), and the second to display the non-null entry of the jacobian matrix.

.. nbplot::

  import numpy as np
  import pylab as pl
  import seaborn as sns
  from functools import partial
  from scipy.sparse import coo_matrix

  def display_jac(J):
      J = (J != 0)
      if J.shape[0] < 20:
            sns.heatmap(J, cbar=False, cmap="gray_r",
                        linecolor="black", linewidths=.1, annot=True)
      else:
          J = coo_matrix(J)
          pl.scatter(J.row, J.col, color="black", marker=".")
          sns.despine(bottom=True, left=True)
          pl.gca().invert_yaxis()

  def approx_fac(F, U, eps=1E-9):
      jac = np.zeros((U.size, U.size))
      eps = 1E-9
      for i in range(U.size):
          U_p = U.copy()
          U_m = U.copy()
          U_p[i] += eps
          U_m[i] -= eps
          jac[:, i] = (F(U_p) - F(U_m)) / (2 * eps)
      return jac

We will first try our first *naive* approach :

.. nbplot::

  def F_naive(U, dx):
      u = U[:U.size//2]
      v = U[U.size//2:]
      dxx = lambda U: (np.roll(U, 1) - 2 * U + np.roll(U, -1)) / dx**2
      return np.concatenate([dxx(v) * dxx(u), dxx(v) + dxx(u)])

  x, dx = np.linspace(-1, 1, 8, retstep=True)
  u = x ** 2
  v = np.cos(x)

  U = np.concatenate([u, v])
  naive_jac = approx_fac(partial(F_naive, dx=dx), U, eps=1E-12)

  display_jac(naive_jac)
  pl.axes().set_aspect('equal')

As we can see, we have three diagonal that appears, with extra values that
appears due to the periodic boundary condition.

As second attempt, we will pile up the unknowns one after the other
*one node at the time* :

.. math::

  U = \begin{bmatrix}
        u_0\\
        v_0\\
        \vdots\\
        u_i\\
        v_i\\
        \vdots\\
        u_N\\
        v_N
      \end{bmatrix}

In that case :

.. nbplot::

  def F_optimized(U, dx):
      u = U[::2]
      v = U[1::2]
      dxx = lambda U: (np.roll(U, 1) - 2 * U + np.roll(U, -1)) / dx**2
      return np.vstack([dxx(v) * dxx(u), dxx(v) + dxx(u)]).flatten("F")

  x, dx = np.linspace(-1, 1, 8, retstep=True)
  u = x ** 2
  v = np.cos(x)

  U = np.vstack([u, v]).flatten("F")
  opt_jac = approx_fac(partial(F_optimized, dx=dx), U, eps=1E-12)

  display_jac(opt_jac)
  pl.axes().set_aspect('equal')


In that case, we have a larger unique band (with extra values due to the
boundary condition). That structure is easier to solve by the sparse solvers
available and will lead to performance improvement.

What we demonstrate for 1D coupled system will be worst for 2D problems.

For a 2D coupled system with periodic boundary condition,

.. math::

  \begin{cases}
    \frac{\partial u}{\partial t} = \partial_{xx}v \times \partial_{xx}u + \partial_{yy}v \times \partial_{yy}u\\
    \frac{\partial v}{\partial t} = \partial_{xx}u + \partial_{xx}v + \partial_{yy}u + \partial_{yy}v
  \end{cases}

we will obtain the following system, with an naive and with an optimized
approach (the latter being computed with scikit-fdiff) :

.. nbplot::

  from skfdiff import Model

  def F_naive2D(U, dx, dy, shape):
      u = U[:U.size//2].reshape(shape)
      v = U[U.size//2:].reshape(shape)
      dxx = lambda U: (np.roll(U, 1, axis=0) - 2 * U + np.roll(U, -1, axis=0)) / dx**2
      dyy = lambda U: (np.roll(U, 1, axis=1) - 2 * U + np.roll(U, -1, axis=1)) / dy**2
      dtu = dxx(v) * dxx(u) + dyy(v) * dyy(u)
      dtv = dxx(v) + dxx(u) + dyy(v) + dyy(u)
      return np.concatenate([dtu, dtv], axis=None)

  model = Model(["dxx(v) * dxx(u) + dyy(v) * dyy(u)",
                 "dxx(v) + dxx(u) + dyy(v) + dyy(u)"],
                ["u(x, y)", "v(x, y)"],
                boundary_conditions="periodic")

  x, dx = np.linspace(-1, 1, 4, retstep=True)
  y, dy = np.linspace(-1, 1, 6, retstep=True)
  xx, yy = np.meshgrid(x, y, indexing="ij")
  u = xx ** 2 + np.exp(yy)
  v = np.cos(xx) * np.sin(yy)

  U = np.concatenate([u, v], axis=None)
  jac = approx_fac(partial(F_naive2D, dx=dx, dy=dy,
                           shape=(x.size, y.size)),
                   U, eps=1E-12)

  fig, (ax1, ax2) = pl.subplots(1, 2, sharey="all")
  pl.sca(ax1)
  display_jac(jac)

  fields = model.Fields(x=x, y=y, u=u, v=v)

  jac = model.J(fields).A

  pl.sca(ax2)
  display_jac(jac)

  ax1.set_aspect('equal')
  ax2.set_aspect('equal')
  pl.tight_layout()

If we compare the time spent for solving the stationary solution with the two
different way to do the vectorization:

.. image:: ../_static/bench_grid.svg

The model has to provide the routine that transform the :py:class:`Fields`,
which are a collection of multidimensional array, to a column vector, and the
inverse transform : the :py:meth:`Model.U_from_fields` and
:py:meth:`Model.fields_from_U`. The grid builder is designed to provide this
two methods in a way that optimize the jacobian structure in a way that improve
the solving of the resulting linear system.
