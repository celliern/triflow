Contribute to scikit-fdiff
==========================

All the contributor can look at the source repository (which is hosted
in `gitlab <https://gitlab.com/celliern/scikit-fdiff/>`_).

Feel free to fork the repository to play with it. You can emit suggestion 
in the `issue tracker  <https://gitlab.com/celliern/
scikit-fdiff/issues>`_.

Reporting issues
----------------

- **Search for existing issues.** Please check to see if someone else has
  reported the same issue.
- **Share as much information as possible.** Include operating system and
  version, browser and version. Also, include steps to reproduce the bug.

Pull requests
-------------

You can also do a pull request to propose an enhancement or a fix. In that
case, you should use `black` as formatter, and run `pre-commit <https://github
.com/pre-commit/pre-commit>`_ to ensure the normalisation of the code. It will
run isort, black and flake8.

Be also sure that all the test are running, by running pytest in the root
folder.
