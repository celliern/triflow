News / TODO / Roadmap
=====================

News
----

v0.7.0: Some improvement, but mainly "quality of life" update.

Fixes:

- Readme update (https://gitlab.com/celliern/scikit-fdiff/issues/3)
- Filter irrelevant warning (https://gitlab.com/celliern/scikit-fdiff/issues/4)
- Fix error on model `__repr__` method
- change software name in pypi for the scikit list automatic parsing.
- fix keep-in-form derivatives (allowing to keep "Dx(k(x) * Dx(U(x)))"
  structure during the spatial discretisation).

New features

- cse for numpy backend (speed-up for complex functions)
- custom function register for numpy backend (see doc and cookbook)
- New container backend using Zarr (better performances, much simpler
  implementation, but still experimental).
- model check, ensuring that no symbol can be forgotten before the model
  evaluation.

.. create this cookbook

v0.6.0: This is a huge step for the software, and a lot of the API modification
is expected. Beside that, the software name change (other software named
triflow were there before this one). All change will not be documented : they
are numerous and it should be considered as a new, more capable and performant
software.

As the main improvement :

- Real arbitrary boundary condition: complex boundary conditions are properly
  dealt with and the domain where they should be applied are automatically
  detected by the solver.
- Arbitrary dimension: the software is now able to deal with 2D / 3D (and more)
  and not only 1D cases.
- Auxiliary definition allow the user to define intermediary variables, making
  the model writing easier.
- The core dependencies have been decreased, and there is the possibility the
  select some extra dependency. In particular, theano is not used anymore,
  allowing easier installation (especially for windows users).
- skfdiff[interactive] allow interactive work with the jupyter notebook.
- skfdiff[numba] make the numba backend available if the non-python dep of
  numba are available on the computer (see the numba doc).

v0.5.1:

- remove some model arguments (simplify, fdiff_jac) that was undocumented.
- make Simulation, Fields, Container, Model pickables in order to improve
  multiprocessing usage of the library

v0.5.0:

- WARNING: some part of the API has changed:

  - Simulation signature has changed. ``t`` arg is now optional (with t=0) as
    default and ``physical_parameters`` is now ``parameters``.
  - The displays have been completely rewritten, and the previous API is
    depreciated. Users are encouraged to modify their scripts or to stick to
    the ^0.4 skfdiff versions.

- move schemes from plugins to core
- backends: remove tensorflow, add numpy which is way slower but has no
  compilation overhead.
- displays and containers are connected to the simulation via `streamz`
- add post-processing.
- real-time display is now based on [Holoviews](https://holoviews.org/).
  Backward compatibility for display is broken and users are encouraged
  to modify their scripts or to stick to the ^0.4 skfdiff versions.
- use poetry to manage dependencies.
- use `tqdm` to display simulation update.

v0.4.12:

- give user choice of backend

  - get out tensorflow backend (not really efficient for increased maintenance
    cost).
  - give access to theano and numpy backend

- upwind scheme support
- using xarray as fields backend, allowing easy post process and save
- update display and containers
- adding repr string to all major classes

v0.4.7:

- adding tensor flow support with full testing
- adding post-processing in bokeh fields display

Roadmap / TODO list
-------------------


The following items are linked to a better use of solid external libs:

- better use of external solving lib:

  - merge skfdiff.plugins.schemes and scipy.integrate.OdeSolver API
  - *use scipy.integrate.solve_ivp for skfdiff temporal scheme solving
    (making it more robust)*
  - main goal is to have better two-way integration with scipy

These are linked to the skfdiff core

- allow heterogeneous grid (variable with different dimensions)
  - aggregation function (mean, integrate, min/max, localized probe)

- allow a choice on the finite difference scheme, on a global way or term by
  term (core implemented, need user api)
- test and propose other backends (Cython, *numba*, pythran?)
- work on adaptive spatial and temporal mesh

These are far away but can be very interesting:

- implement continuation algorithm working with skfdiff (separate project?)
