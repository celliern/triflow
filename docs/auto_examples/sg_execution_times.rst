
:orphan:

.. _sphx_glr_auto_examples_sg_execution_times:

Computation times
=================
**00:17.657** total execution time for **auto_examples** files:

- **00:17.657**: :ref:`sphx_glr_auto_examples_plot_1D_kdv.py` (``plot_1D_kdv.py``)
- **00:00.000**: :ref:`sphx_glr_auto_examples_2D_acoustic_case.py` (``2D_acoustic_case.py``)
- **00:00.000**: :ref:`sphx_glr_auto_examples_2D_dam_break.py` (``2D_dam_break.py``)
- **00:00.000**: :ref:`sphx_glr_auto_examples_plot_1D_dambreak.py` (``plot_1D_dambreak.py``)
- **00:00.000**: :ref:`sphx_glr_auto_examples_plot_1D_steady_lake.py` (``plot_1D_steady_lake.py``)
